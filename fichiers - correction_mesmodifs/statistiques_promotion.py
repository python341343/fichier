'''
    Programme de lecture et d'analyse d'un fichier structuré (CSV)
    statistuqes : H/F de la promotion
    Version actuelle : 1.2
'''

import argparse
import datetime as dt
from dateutil import relativedelta as rd
# Import des fonctons de l'application
import functions.application_functions as af

# Définir la version du script
version = "1.2"

# Ajouter le support de la version en tant qu'argument
parser = argparse.ArgumentParser(description="Programme de lecture et d'analyse d'un fichier structuré (CSV)")
parser.add_argument("-f", "--path", type=str)
parser.add_argument("-v", "--version", action="store_true", help="Afficher la version du programme")

args = parser.parse_args()

if args.version:
    print(f"Version actuelle : {version}")
    exit()

file = args.path

if file == None:
    file = "promotion_B3_B.csv"

today = dt.datetime.now()

# Boucle de lecture ligne à ligne
with open(file, 'r') as file:
    nbrf = nbrh = nbro = 0
    ages = []  # Pour calculer l'âge moyen
    # On saute la premier ligne
    next(file)
    for line in file:
        if line[0] == '#':
            continue
        fields = line.strip().split(";")
        gender = fields[2]
        bdate = fields[4]
        bdate = dt.datetime.strptime(bdate, '%d/%m/%Y')
        ans, mois = af.gdelta_age(today, bdate)
        print(f"{fields[1]} - {fields[0]} - {bdate} , âge : {ans} ans et {mois} mois")
        if gender.lower() == 'h':
            nbrh += 1
        elif gender.lower() == 'f':
            nbrf += 1
        else:
            nbro += 1
        ages.append(ans * 12 + mois)  # Convertir l'âge en mois pour le calcul de la moyenne

nbr_tot = nbrf + nbrh + nbro
age_moyen = sum(ages) / len(ages) if ages else 0  # Calcul de l'âge moyen
ans_moyen = int(age_moyen // 12)
mois_moyen = int(age_moyen % 12)

sep = "=" * 80
print(sep)
print(f"Nombre total d'élèves : {nbr_tot}")
print(f"Nombre total de filles : {nbrf} - {af.cpercent(nbrf, nbr_tot, 2)} %")
print(f"Nombre total de garçons : {nbrh} - {af.cpercent(nbrh, nbr_tot, 2)} %")
print(f"Autres : {nbro} - {af.cpercent(nbro, nbr_tot, 2)} %")
print(f"Age moyen de la promotion : {ans_moyen} ans et {mois_moyen} mois")
print(sep)
print("Fin du programme...")
