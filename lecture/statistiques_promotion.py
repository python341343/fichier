import argparse
from datetime import datetime, date

def calculate_age(born, reference_date):
    return reference_date.year - born.year - ((reference_date.month, reference_date.day) < (born.month, born.day))

def calculate_age_factorized(birthday):
    today = date.today()
    return calculate_age(birthday, today)

def process_file(file_path):
    nbrf = nbrh = nbro = 0
    nbr_tot = max(1, nbrf + nbrh + nbro)

    # Définir today en dehors de la boucle
    today = date.today()

    # Boucle de lecture ligne à ligne
    with open(file_path, 'r') as file:
        # On saute la première ligne
        next(file)
        for line in file:
            fields = line.split(";")
            gender = fields[2]

            # Vérification si la colonne contient une date au format DD/MM/YYYY
            try:
                fields = line.split(";")
                birthday = datetime.strptime(fields[3], '%d/%m/%Y').date()
            except ValueError:
                # Si la conversion échoue, imprime un message d'erreur et continue avec la prochaine ligne
                print(f"Erreur de conversion de date pour la ligne : {line.strip()}")
                continue

            age = calculate_age_factorized(birthday)
            print(f"{fields[1]} - {fields[0]} - genre: {gender} - age: {age} ans et {today.month - birthday.month} mois")

            if gender.lower() == 'h':
                nbrh += 1
            elif gender.lower() == 'f':
                nbrf += 1
            else:
                nbro += 1

    

    nbr_tot = max(1, nbrf + nbrh + nbro)
    sep = "=" * 80
    print(sep)
    print(f"Nombre total d'élèves : {nbr_tot}")
    print(f"Nombre total de filles : {nbrf} - {round((nbrf / nbr_tot) * 100, 2)} %")
    print(f"Nombre total de garçons : {nbrh} - {round((nbrh / nbr_tot) * 100, 2)} %")
    print(f"Autres : {nbro} - {round((nbro / nbr_tot) * 100, 2)} %")
    print(sep)




if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--path", type=str)

    args = parser.parse_args()
    file = args.path

    if file is None:
        file = "promotion_B3_B.csv"

    process_file(file)
    print("Fin du programme...")
