import argparse

somme = 0
sommefille = 0
sommegarcon = 0 
sommepasdesexe = 0

def parse_arguments():
    parser = argparse.ArgumentParser(description="Analyser un fichier CSV d'étudiants.")
    parser.add_argument("-f", "--path", type=str, help="Chemin du fichier CSV")
    return parser.parse_args()

args = parse_arguments()

try:
    with open(args.path, 'r') as file:
        next(file) 
        for line in file:
            fields = line.split(";")
            somme += 1  
            genre = fields[2].lower()
            if genre == 'h':
                sommegarcon += 1  
            elif genre == 'f':
                sommefille += 1 
            else:
                sommepasdesexe += 1  # Incrementer le nombre d'étudiants sans genre spécifié

except FileNotFoundError:
    print(f"Erreur : Le fichier spécifié ({args.path}) n'a pas été trouvé. Veuillez vérifier le chemin du fichier.")
except Exception as e:
    print(f"Erreur : {e}")

# Calcul des pourcentages
pourcentage_garcons = (sommegarcon / somme) * 100
pourcentage_filles = (sommefille / somme) * 100
pourcentage_pas_de_sexe = (sommepasdesexe / somme) * 100

print(f"La salle d'étudiant est constituée de : {somme} étudiant(e)s au total.")
print(f"La salle d'étudiant est constituée de : {sommegarcon} garçon(s) ({pourcentage_garcons:.2f}%)")
print(f"La salle d'étudiant est constituée de : {sommefille} fille(s) ({pourcentage_filles:.2f}%)")
print(f"La salle d'étudiant est constituée de : {sommepasdesexe} sans genre(s) ({pourcentage_pas_de_sexe:.2f}%)")
