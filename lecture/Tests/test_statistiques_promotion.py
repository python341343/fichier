import sys
sys.path.append("..")
from lecture.statistique_promotion import calculate_age, is_major, get_age_bounds

import unittest
from datetime import date, timedelta

class TestStatistiquesPromotion(unittest.TestCase):

    def test_calculate_age(self):
        reference_date = date(2023, 1, 1)
        birthday = date(2000, 1, 1)
        self.assertEqual(calculate_age(birthday, reference_date), 23)

    def test_is_major(self):
        reference_date = date.today()
        birthday_major = reference_date - timedelta(days=365 * 18)
        birthday_minor = reference_date - timedelta(days=365 * 16)
        self.assertTrue(is_major(birthday_major, reference_date))
        self.assertFalse(is_major(birthday_minor, reference_date))

    def test_get_age_bounds(self):
        reference_date = date.today()
        birthday = reference_date - timedelta(days=365 * 18)
        bounds = get_age_bounds(birthday, reference_date)
        self.assertEqual(bounds, (17, 18, 19))

if __name__ == '__main__':
    unittest.main()
